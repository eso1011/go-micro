# Compile stage
FROM golang:1.13.7-alpine AS build-env

# Go binary installed in Docker images is not built with
# CGO_ENABLED=0. "go vet" was failing for the programs that
# imported packages with cgo files.
# https://go-review.googlesource.com/c/playground/+/123136/

ENV CGO_ENABLED=0
#copy source
COPY ./src /go/bin/api

#compile binary file (matterbridge)
WORKDIR /go/bin/api
RUN go build


## Final stage
FROM alpine

WORKDIR /var/www/ultraticket/
#copy binary file
COPY --from=build-env /go/bin/api/ultra-ticket-api .

COPY --from=build-env /go/bin/api/build ./build

CMD ["./ultra-ticket-api"]

EXPOSE 8000

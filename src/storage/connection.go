package storage

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"os"
)

func GetConnection()(conn *pgx.Conn){
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	dbPass := os.Getenv("DB_PASS")
	dbUser :=  os.Getenv("DB_USER")

	connection := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", dbUser, dbPass, dbHost, dbName)
	conn, err := pgx.Connect(context.Background(), connection)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to connection to database: %v\n", err)
	}

	return conn

}

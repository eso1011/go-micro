package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/micro/go-micro/web"
	"log"
	"net/http"
	"ultra-ticket-api/storage"
)

type Product struct {
	Id   int
	Name string
}

func ProductHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	conn := storage.GetConnection()
	defer conn.Close(context.Background())

	rows, err := conn.Query(
		context.Background(),
		"select id, name from products where id > $1", vars["product"])

	if err != nil {
		_, _ = fmt.Fprintf(w, "QueryRow failed: %v\n", err)
	}

	var products []Product
	var product Product
	for rows.Next() {
		_ = rows.Scan(&product.Id, &product.Name)
		products = append(products, product)
	}

	prettyJson, _ := json.MarshalIndent(products, "", "    ")
	_, _ = fmt.Fprintf(w, string(prettyJson))
}

func main() {
	router := mux.NewRouter()

	subrouter := router.PathPrefix("/api").Subrouter()

	subrouter.HandleFunc("/products/{product}", ProductHandler)

	var dir string

	flag.StringVar(&dir, "dir", ".", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	service := web.NewService(
		web.Name("go.micro.api.greeter"),
		web.Handler(router),
		web.Address("0.0.0.0:8000"),
	)
	_ = service.Init()

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

	//log.Fatal(srv.())
}

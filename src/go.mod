module ultra-ticket-api

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/jackc/pgx/v4 v4.4.0
	github.com/micro/go-micro v1.18.0
)
